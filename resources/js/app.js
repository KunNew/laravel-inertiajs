require('./bootstrap');

import { createApp, h } from 'vue'
import { createInertiaApp, Head, Link } from '@inertiajs/inertia-vue3'
import { InertiaProgress } from '@inertiajs/progress'
import Layout from '../js/Layouts/Layout.vue'

createInertiaApp({
  resolve: async name => {
//    let page = require(`./Pages/${name}`).default
let page = (await import(`./Pages/${name}`)).default
//    if (!page.layout) {
//     page.layout = Layout
//    }
//    return page;
// page.layout ??= Layout;
if(page.layout === undefined) {
    page.layout = Layout
}
return page;
  },
  setup({ el, App, props, plugin }) {
    createApp({ render: () => h(App, props) })
      .use(plugin).component("Link",Link)
      .mount(el)
  },
 title: () => "My APP"
})

InertiaProgress.init({
    color: 'red',
    delay: 2,
    showSpinner: true
})
