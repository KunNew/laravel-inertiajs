<?php

use App\Http\Controllers\LoginController;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', [LoginController::class, 'create'])->name('login');

Route::post('/login', [LoginController::class, 'store']);
Route::middleware('auth')->group(function () {



Route::get('/', function () {
    return Inertia::render('Welcome',['name' => "Vue JS",'framwork' => ['Vue','React'],'username'=> "Pheak Minute."]);
});
Route::get('/setting', function () {
return Inertia::render('Setting');
});
Route::get('/user', function () {
    // return User::paginate(10);
    return Inertia::render('User',['users' => User::query()->when(Request::input('search'), function($query,$search) {
        $query->where('name','like','%'. $search. '%');
    })->
    paginate(10)->withQueryString()->through(fn($user) => [
        'id' => $user->id,
        'name' => $user->name
    ]),
    'filters' => Request::only(['search']),
    'can' => [
        'createUser' => Auth::user()->email == 'pov@gmail.com'
    ]
]);

    });
    Route::get('/create-user', function () {
        // return User::paginate(10);
        return Inertia::render('user/create');

        });
        Route::post('/user', function () {
            // return User::paginate(10);
            $data = Request::validate([
                'name' => 'required',
                'email' => 'required| email',
                'password' => 'required|min:8',
            ]);
            User::create($data);
            return redirect('/user');

            });
Route::post('/logout', function () {
       dd(request('foo'));
        });
Route::get('/remove-user/{$id}', function($id) {
     //find post by ID
     $user = User::find($id);

     //delete post
     $user->delete();
     return redirect('/user');
});

Route::post('/logout', [LoginController::class, 'logout']);
});
